package game;

public abstract class Goal {

    private Game game;
    private final String description;
    private final String name;

    /**
     * Constructs a new goal/mission
     * @param name Quick name
     * @param description Description for the goal
     */
    public Goal(String name, String description) {
        this.name = name;
        this.description = description;
    }

    /**
     * Set the current game
     * @param game The game
     */
    public void setGame(Game game) {
        this.game = game;
    }

    /**
     * Returns whether the goal/mission is completed
     * @return True if the mission is complete, false
     * otherwise
     */
    public abstract boolean isCompleted();
    
    /**
     * Returns the winner of the game or null
     * if there is no winner (yet)
     * @return A Player object or null
     */
    public abstract Player getWinner();
    
    /**
     * Returns whether a player has lost
     * @param player The player
     * @return True if the player has lost
     */
    public abstract boolean hasLost(Player player);

    /**
     * Gets the description of the mission
     * @return The description
     */
    public final String getDescription() {
        return this.description;
    }

    /**
     * Gets the name of the mission
     * @return The name
     */
    public final String getName() {
        return this.name;
    }

    /**
     * Gets the game attached to this mission
     * @return The game
     */
    protected Game getGame() {
        return this.game;
    }
}
