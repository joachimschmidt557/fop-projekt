package game.map;

import game.Player;

import java.util.LinkedList;
import java.util.List;

/**
 * Diese Klasse representiert ein Königreich. Jedes Königreich hat eine Liste von Burgen sowie einen Index {@link #type} im Bereich von 0-5
 *
 */
public class Kingdom {

    private List<Castle> castles;
    private Castle center;
    private int type;

    /**
     * Erstellt ein neues Königreich
     * @param type der Typ des Königreichs (im Bereich 0-5)
     * @param center das Zentrum des Königreichs
     */
    public Kingdom(int type, Castle center) {
        this.castles = new LinkedList<>();
        this.type = type;
        this.center = center;
        center.setKingdom(this);
    }

    /**
     * Eine Burg zum Königreich hinzufügen
     * @param castle die Burg, die hinzugefügt werden soll
     */
    public void addCastle(Castle castle) {
    	if(!this.castles.contains(castle))
    		this.castles.add(castle);
    }
    
    /**
     * Eine Burg als Zentrum des Königreichs festlegen
     * @param castle die Burg, die als Zentrum festgelegt werden soll
     */
    public void setCenter(Castle castle) {
        this.center = castle;
    }
    
    /**
     * Gibt das Zentrum des Königreichs zurück
     * @return castle die Burg, die das Zentrum des Königreichs darstellt
     */
    public Castle getCenter() {
        return this.center;
    }

    /**
     * Gibt den Typen des Königreichs zurück. Dies wird zur korrekten Anzeige benötigt
     * @return der Typ des Königreichs.
     */
    public int getType() {
        return this.type;
    }

    /**
     * Eine Burg aus dem Königreich entfernen
     * @param castle die zu entfernende Burg
     */
    public void removeCastle(Castle castle) {
        this.castles.remove(castle);
    }

    /**
     * Gibt den Spieler zurück, der alle Burgen in dem Köngreich besitzt.
     * Sollte es keinen Spieler geben, der alle Burgen besitzt, wird null zurückgegeben.
     * @return der Besitzer oder null
     */
    public Player getOwner() {
        if(castles.isEmpty())
            return null;

        Player owner = castles.get(0).getOwner();
        for(Castle castle : castles) {
            if(castle.getOwner() != owner)
                return null;
        }

        return owner;
    }

    /**
     * Gibt alle Burgen zurück, die in diesem Königreich liegen
     * @return Liste von Burgen im Königreich
     */
    public List<Castle> getCastles() {
        return this.castles;
    }
}
