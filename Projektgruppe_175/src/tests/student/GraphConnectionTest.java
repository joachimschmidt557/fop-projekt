package tests.student;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

import base.*;

public class GraphConnectionTest {

	@Test
	public void testCorrectGraph() {
		
		Graph<String> testGraph = new Graph<String>();
		
		Node<String> a = testGraph.addNode("A");
		Node<String> b = testGraph.addNode("B");
		Node<String> c = testGraph.addNode("C");
		Node<String> d = testGraph.addNode("D");
		
		testGraph.addEdge(a, b);
		testGraph.addEdge(a, c);
		testGraph.addEdge(d, c);
		
		assertTrue(testGraph.allNodesConnected());
		
	}
	
	@Test
	public void testIncorrectGraph() {
		
		Graph<Integer> testGraph = new Graph<Integer>();
		
		Node<Integer> one = testGraph.addNode(1);
		Node<Integer> two = testGraph.addNode(2);
		Node<Integer> four = testGraph.addNode(4);
		Node<Integer> seven = testGraph.addNode(7);
		Node<Integer> nine = testGraph.addNode(9);
		
		testGraph.addEdge(one, seven);
		testGraph.addEdge(two, nine);
		
		assertFalse(testGraph.allNodesConnected());
		
	}
	
}
