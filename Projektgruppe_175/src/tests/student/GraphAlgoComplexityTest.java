package tests.student;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import base.Edge;
import base.Graph;
import base.GraphAlgorithm;
import base.Node;

public class GraphAlgoComplexityTest {

	@Test
	public void test() {
		
		for (int i = 30; i < 50; i+=1) {
			
			/*
			
			Graph<Integer> g = generateGraph(i);
			DumbGraphAlgorithm ga = new DumbGraphAlgorithm(g, g.getNodes().get(0));
			
			ga.run();
			
			double divN = (double)ga.getN() / (double)i;
			double divN2 = (double)ga.getN() / ((double)i*i);
			double divN3 = (double)ga.getN() / ((double)i*i*i);
			
			System.out.println(i + "," + ga.getN() + "," + divN + "," + divN2 + "," + divN3);
			
			*/
			
			/////////////
			
			Graph<Integer> g2 = generateGraph2(i);
			DumbGraphAlgorithm ga2 = new DumbGraphAlgorithm(g2, g2.getNodes().get(4));
			
			ga2.run();
			
			List<Edge<Integer>> path = ga2.getPath(g2.getNodes().get(2));
			for (Edge<Integer> edge : path) {
				System.out.println(edge.getNodeA().getValue().toString() + " " + edge.getNodeB().getValue().toString());
			}
			
			//System.out.println();
			
		}
		
	}
	
	public Graph<Integer> generateGraph(int n) {
		
		Graph<Integer> g = new Graph<Integer>();
		
		List<Node<Integer>> nodes = new ArrayList<>();
		
		for (int i = 0; i < n; i++)
			nodes.add(g.addNode(i));
		
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i != j)
					g.addEdge(nodes.get(i), nodes.get(j));
			}
		}
		
		return g;
		
	}
	
	public Graph<Integer> generateGraph2(int n) {
		
		Graph<Integer> g = new Graph<Integer>();
		
		List<Node<Integer>> nodes = new ArrayList<>();
		
		for (int i = 0; i < n; i++)
			nodes.add(g.addNode(i));
		
		for (int i = 0; i < n - 1; i++) {
			g.addEdge(nodes.get(i), nodes.get(i+1));
		}
		
		g.addEdge(nodes.get(6), nodes.get(9));
		
		return g;
		
	}
	
	public class DumbGraphAlgorithm extends GraphAlgorithm<Integer> {

		public DumbGraphAlgorithm(Graph<Integer> graph, Node<Integer> sourceNode) {
			super(graph, sourceNode);
		}

		@Override
		protected double getValue(Edge<Integer> edge) {
			return 100.0;
		}

		@Override
		protected boolean isPassable(Edge<Integer> edge) {
			return true;
		}

		@Override
		protected boolean isPassable(Node<Integer> node) {
			return true;
		}
		
	}
	
}
