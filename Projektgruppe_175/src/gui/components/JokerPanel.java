package gui.components;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;
import javax.swing.JOptionPane;

import game.Game;
import game.map.Castle;
import gui.Resources;
import gui.views.GameView;

public class JokerPanel extends JPanel implements MouseListener{

	private Resources resources;
	private Game game;
	
	private MapPanel map;
	
	private GameView gv;
	
	public enum JokerTypes {
			ADD_TROOPS,
			ADD_TROOPS_USED,
			SCARE_TROOPS,
			SCARE_TROOPS_USED,
	}
	
	private JokerTypes[] jokers;
	
	/**
	 * Constructs a new joker panel
	 * @param resources The resources
	 * @param game The corresponding game
	 */
	public JokerPanel(Resources resources, Game game) {
		this.game = game;
		this.resources = resources;
		this.addMouseListener(this);
		initJokers();
	}
	
	/**
	 * Initializes the jokers
	 */
	private void initJokers() {
		jokers = new JokerTypes[]{JokerTypes.ADD_TROOPS, JokerTypes.SCARE_TROOPS };
	}
	
	/**
	 * Associates with a game view
	 * @param gv The game view
	 */
	public void setGameView(GameView gv) {
		this.gv = gv;
	}

	/**
	 * Associates with a game
	 * @param game The game
	 */
	public void setGame(Game game) {
		this.game = game;
	}
	
	
	public void setJokers(JokerTypes[] jokers) {
		this.jokers = jokers;
	}
	
	/**
	 * Associates with a map panel
	 * @param map The map panel
	 */
	public void setMapPanel(MapPanel map) {
		this.map = map;
	}
	
	/**
	 * Modifies the joker at this index
	 * @param index The index
	 * @param j The new joker
	 */
	private void setJoker(int index, JokerTypes j) {
		game.getCurrentPlayer().setJoker(index, j);
	}
	
	/**
	 * Plays the joker at this index
	 * @param index The index
	 */
	public void playJoker(int index) {
		if(game.getRound() < 2)
			return;
		
		if(index == 0) {
			
			 int choice = JOptionPane.showOptionDialog(this, "Dieser Joker gibt dir 5 Truppen. Sicher, dass du ihn verwenden willst?",
                     "Truppen-Joker",
                     JOptionPane.YES_NO_OPTION,
                     JOptionPane.INFORMATION_MESSAGE,
                     null, new String[]{"Benutzen", "Abbrechen"}, "Benutzen");
			if(choice == 1)
				return;
			game.getCurrentPlayer().addTroops(5);
			jokers[0] = JokerTypes.ADD_TROOPS_USED;
			gv.updateStats();
			gv.repaint();
		}
		
		if(index == 1) {
			 int choice = JOptionPane.showOptionDialog(this, "Dieser Joker jagt den gegnerischen Truppen in einer Burg deiner Wahl Angst ein, sodass sie in benachbarte Burgen fliehen.\nFalls es keine benachbarten Burgen gibt, rennen die gegnerischen Truppen in den Wald und sterben einen qualvollen Hungertod. Sicher, dass du ihn verwenden willst?",
                     "Truppen verscheuchen",
                     JOptionPane.YES_NO_OPTION,
                     JOptionPane.INFORMATION_MESSAGE,
                     null, new String[]{"Benutzen", "Abbrechen"}, "Benutzen");
			 if(choice == 1)
				 return;
			 
			 map.setIsChoosingJoker(true);
			 game.getCurrentPlayer().setScareTroopsUsed();
			 gv.updateStats();
			 gv.repaint();
		}
	}
	
	@Override
	public void paintComponent(Graphics g) {
        super.paintComponent(g);

        paintJokers(g);
	}

	/**
	 * Paints the jokers
	 * @param g The graphics to paint on
	 */
	public void paintJokers(Graphics g) {
        if(jokers == null)
			return;
		
		
		
		for(int i = 0; i < jokers.length; i++) {
			g.drawImage(resources.getJokers()[game.getCurrentPlayer().getJokers()[i].ordinal()], 9 + 32 * i + 10 * i, 5, 32, 32, null);
		}
		
	}
	

	@Override
	public void mouseClicked(MouseEvent e) {
		if(e.getX() <= 41 && jokers[0] == JokerTypes.ADD_TROOPS) {
			playJoker(0);
		} else if(e.getX() > 50 && e.getX() < 100 && jokers[1] == JokerTypes.SCARE_TROOPS)
			playJoker(1);
		
	}


	@Override
	public void mousePressed(MouseEvent e) {
		
	}


	@Override
	public void mouseReleased(MouseEvent e) {
		
	}


	@Override
	public void mouseEntered(MouseEvent e) {

	}


	@Override
	public void mouseExited(MouseEvent e) {

	}
	
}
